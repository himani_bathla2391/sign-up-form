//
//  ViewController.m
//  login form
//
//  Created by Clicklabs14 on 9/17/15.
//  Copyright (c) 2015 click lab. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UILabel *login;
UILabel *firstname;
UILabel *lastname;
UILabel *email;
UILabel *phno;
UILabel *password;
UILabel *cpassword;
UILabel *cpassword2;
UIButton *button;

UITextField *tfirstname;
UITextField *tlastname;
UITextField *temail;
UITextField *tphno;
UITextField *tpassword;
UITextField *tcpassword;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    login=[[UILabel alloc]initWithFrame:CGRectMake(100, 40, 150, 30)];
    login.text= @"SIGN UP";
    login.textColor= [UIColor whiteColor];
    login.font = [UIFont fontWithName:@"Chalkduster" size:30];
    [self.view addSubview: login];

    
    firstname=[[UILabel alloc]initWithFrame:CGRectMake(40, 120, 90, 30)];
    firstname.text= @"First name";
    firstname.textColor= [UIColor blueColor];
    firstname.font = [UIFont fontWithName:@"Palatino" size:18];
    [self.view addSubview: firstname];
    
    tfirstname= [[UITextField alloc] initWithFrame:CGRectMake(150, 120, 200, 30)];
    tfirstname.placeholder= @"enter firstname";
    tfirstname.borderStyle= UITextBorderStyleRoundedRect;
    [tfirstname setFont:[UIFont boldSystemFontOfSize:18]];
    [self.view addSubview:tfirstname];

    lastname=[[UILabel alloc]initWithFrame:CGRectMake(40, 180, 90, 30)];
    lastname.text= @"Last name";
    lastname.textColor= [UIColor blueColor];
    lastname.font = [UIFont fontWithName:@"Palatino" size:18];
    [self.view addSubview: lastname];
    
    tlastname= [[UITextField alloc] initWithFrame:CGRectMake(150, 180, 200, 30)];
    tlastname.placeholder= @"enter lastname";
    tlastname.borderStyle= UITextBorderStyleRoundedRect;
    [tlastname setFont:[UIFont boldSystemFontOfSize:18]];
    [self.view addSubview:tlastname];
    
    
    email=[[UILabel alloc]initWithFrame:CGRectMake(40, 240, 90, 30)];
    email.text= @"Email";
    email.textColor= [UIColor blueColor];
    email.font = [UIFont fontWithName:@"Palatino" size:18];
    [self.view addSubview: email];
    
    temail= [[UITextField alloc] initWithFrame:CGRectMake(150, 240, 200, 30)];
    temail.placeholder= @"enter email";
    temail.borderStyle= UITextBorderStyleRoundedRect;
    [temail setFont:[UIFont boldSystemFontOfSize:18]];
    [self.view addSubview:temail];
    

    phno=[[UILabel alloc]initWithFrame:CGRectMake(40, 300, 90, 30)];
    phno.text= @"Phone no.";
    phno.textColor= [UIColor blueColor];
    phno.font = [UIFont fontWithName:@"Palatino" size:18];
    [self.view addSubview: phno];
    
    tphno= [[UITextField alloc] initWithFrame:CGRectMake(150, 300, 200, 30)];
    tphno.placeholder= @"enter phonenumber";
    tphno.borderStyle= UITextBorderStyleRoundedRect;
    [tphno setFont:[UIFont boldSystemFontOfSize:18]];
    [self.view addSubview:tphno];
    
    password=[[UILabel alloc]initWithFrame:CGRectMake(40, 360, 90, 30)];
    password.text= @"Password";
    password.textColor= [UIColor blueColor];
    password.font = [UIFont fontWithName:@"Palatino" size:18];
    [self.view addSubview: password];
    
    tpassword= [[UITextField alloc] initWithFrame:CGRectMake(150, 360, 200, 30)];
    tpassword.placeholder= @"enter password";
    tpassword.borderStyle= UITextBorderStyleRoundedRect;
    [tpassword setFont:[UIFont boldSystemFontOfSize:18]];
    [tpassword setSecureTextEntry:YES];
    [self.view addSubview:tpassword];
    
    cpassword=[[UILabel alloc]initWithFrame:CGRectMake(40, 420, 90, 30)];
    cpassword.text= @"Retype";
    cpassword.textColor= [UIColor blueColor];
     cpassword.font = [UIFont fontWithName:@"Palatino" size:18];
    [self.view addSubview: cpassword];
    
    cpassword2=[[UILabel alloc]initWithFrame:CGRectMake(40, 434, 90, 30)];
    cpassword2.text= @"password";
    cpassword2.textColor= [UIColor blueColor];
    cpassword2.font = [UIFont fontWithName:@"Palatino" size:18];
    [self.view addSubview: cpassword2];
    
    
    tcpassword= [[UITextField alloc] initWithFrame:CGRectMake(150, 420, 200, 30)];
    tcpassword.placeholder= @"enter password again";
    tcpassword.borderStyle= UITextBorderStyleRoundedRect;
    [tcpassword setFont:[UIFont boldSystemFontOfSize:18]];
    [tcpassword setSecureTextEntry:YES];
    [self.view addSubview:tcpassword];
    
    button= [[UIButton alloc] initWithFrame:CGRectMake(100, 450, 150, 180)];
    [button setTitle:@"Submit" forState:(UIControlStateNormal)];
    [button addTarget: self action : @selector(Submit:) forControlEvents:(UIControlEventTouchUpInside)];
    button.font = [UIFont fontWithName:@"Chalkduster" size:30];
    [self.view addSubview: button];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"images.jpg"]]];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)Submit: (UIButton *) sender{
    
    if ([tfirstname.text length] == 0)
    {
        
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your first name"  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
        }
    else if ([tlastname.text length] ==0)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your last name"   preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([temail.text length] ==0)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your email"   preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    else if ([tphno.text length] == 0)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your phone number"  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([tpassword.text length] ==0)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your password"  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([tcpassword.text length] ==0)
    {
        
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"All fields are mandatory"  message:@"Enter your confirmation password "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([self check: tfirstname.text ] == YES)
    {
        tfirstname.textColor= [UIColor redColor];
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"You have entered digits in first name"  message:@" Enter characters "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }

    else if ([self check: tlastname.text ] == YES)
    {
        tlastname.textColor= [UIColor redColor];
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"You have entered digits in last name"  message:@"Enter characters "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
   
    else if ([self check2: tphno.text ] == YES)
    {
        tphno.textColor= [UIColor redColor];
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"You have entered character in phone number "  message:@"Enter digits "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];

    }
    else if ([self check3: tpassword.text ] == YES)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"wrong input password"  message:@"passsword should contain digits ,characters and special character(@,#,$,%,&) "  preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }

    else if ([tphno.text length] < 10)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Phone number must be of 10 digits "  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }

    
    else if ([tphno.text length] > 10)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Phone number must be of 10 digits only"  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([tpassword.text length] < 8)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Password must be 8 characters long "  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }

    
    else if ([tpassword.text length] > 8)
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Password must be 8 characters long only "  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }

    else if ([tpassword.text isEqualToString: tcpassword.text])
    {
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"You have been registered successfully"  message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
   else {
       UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Error in registeration "  message:nil preferredStyle:UIAlertControllerStyleAlert];
       [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
           [self dismissViewControllerAnimated:YES completion:nil];
       }]];
       [self presentViewController:alertController animated:YES completion:nil];
        }
    
}

-(BOOL) check: (NSString*) text{
    NSCharacterSet *set =[NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    if([text rangeOfCharacterFromSet:set].location== NSNotFound )
    {
        return NO;
    }
    else
    {
        return YES;
    }
}


-(BOOL) check2: (NSString*) text{
    NSCharacterSet *set =[[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]invertedSet];
    if([text rangeOfCharacterFromSet:set].location== NSNotFound )
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
-(BOOL) check3: (NSString*) text{
    NSCharacterSet *set =[[NSCharacterSet characterSetWithCharactersInString:@"1234567890QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm@#$%&"]invertedSet];
    if([text rangeOfCharacterFromSet:set].location== NSNotFound )
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
